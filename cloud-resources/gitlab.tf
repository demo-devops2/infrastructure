data "google_project" "s4nhub_project" {
  project_id = var.gcp_project_id
}

resource "gitlab_project_cluster" "infra-cluster-k8s" {
  provider           = gitlab
  project            = var.gitlab_infrastructure_project_id
  name               = module.demo_devops_cluster.name
  environment_scope  = "${terraform.workspace}/*"
  managed            = false
  kubernetes_api_url = "https://${module.demo_devops_cluster.endpoint}"
  kubernetes_token   = data.google_client_config.default.access_token
  kubernetes_ca_cert = trimspace(
    base64decode(
      module.demo_devops_cluster.ca_certificate,
    )
  )

  # The kubernetes_token, is obtained from a data resource which 
  # is always refreshed
  lifecycle {
    ignore_changes = [
      kubernetes_token
    ]
  }
}

# --------------------------------------------
# Gitlab deploy key for infrastructure project
# --------------------------------------------
resource "gitlab_deploy_key" "flux_infrastructure_project" {
  project  = var.gitlab_infrastructure_project_id
  title    = "Flux Infrastructure Deploy Key - ${terraform.workspace}"
  key      = kubernetes_secret.flux_git_deploy.data["public"]
  can_push = true
  depends_on = [
    kubernetes_secret.flux_git_deploy
  ]

  lifecycle {
    ignore_changes = [key]
  }
}

# -----------------------------------------------------------------
# Enable Infrastructure deploy key in microservice releases project
# -----------------------------------------------------------------
resource "gitlab_deploy_key_enable" "flux_microservice_releases" {
  project = var.gitlab_microservice_releases_project_id
  key_id  = gitlab_deploy_key.flux_infrastructure_project.id
}
