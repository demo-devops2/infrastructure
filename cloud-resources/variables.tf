variable "gcp_project_id" {
  description = "The project ID to host the cluster in"
}

variable "gcp_region" {
  description = "The region to host the cluster in"
  default     = "us-east1"
}

variable "gcp_vpc_network" {
  description = "The VPC network created to host the cluster in"
  default     = "gke-network"
}

variable "gcp_vpc_subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "gke-subnet"
}

variable "gcp_vpc_ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}

variable "gcp_vpc_ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-svc"
}

variable "gke_cluster_name" {
  description = "The name for the GKE cluster"
  default     = "demo-devops-cluster"
}

variable "gke_istio" {
  type        = bool
  description = "Boolean to enable / disable Istio"
  default     = false
}

variable "gke_istio_strict_mtls" {
  type        = bool
  description = "Boolean to enable / disable Istio Strict MTLS"
  default     = false
}

variable "gke_logging_and_monitoring" {
  type        = bool
  description = "Logging and monitoring services for GKE cluster (none | )"
  default     = false
}

variable "gke_network_policy" {
  type        = bool
  description = "Boolean to enable / disable network policy"
  default     = true
}

variable "gke_preemtible" {
  description = "Boolean to enable / disable Preemtible GCP virtual machines"
  default     = true
}

variable "gitlab_infrastructure_project_id" {
  type        = number
  description = "The project ID to which this cluster should be associated"
  default     = 21387888
}

variable "gitlab_microservice_releases_project_id" {
  type        = number
  description = "The project of the iac for the namespaces on the cluster"
  default     = 21387892
}

variable "flux_cluster_git_branch" {
  type        = string
  description = "The branch of the infrastructure git repo, Flux should sync"
  default     = "master"
}

variable "flux_cluster_git_path" {
  type        = string
  description = "The path of the infrastructure git repo, Flux should sync"
  default     = "cluster-resources/pre-production"
}

variable "flux_microservice_releases_git_branch" {
  type        = string
  description = "The branch of the microservice releases git repo, Flux should sync"
  default     = "master"
}

variable "gitlab_registry_username" {
  description = "Gitlab private registry username"
}

variable "gitlab_registry_password" {
  description = "Gitlab private registry password"
}
