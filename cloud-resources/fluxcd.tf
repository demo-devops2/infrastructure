locals {
  git_secret_name = "flux-git-deploy"
}

# -------------------------
# Flux namespace
# -------------------------
resource "kubernetes_namespace" "flux_system" {
  metadata {
    name = "flux-system"
  }
  lifecycle {
    ignore_changes = [
      metadata,
    ]
  }
  depends_on = [
    module.demo_devops_cluster
  ]
}

# ------------------------------------------------------
# Private key for flux connection to Gitlab Repositories
# ------------------------------------------------------
resource "tls_private_key" "flux_ssh_key" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

# ------------------------------------------------------------------
# Secret with private key for flux connection to Gitlab Repositories
# ------------------------------------------------------------------
resource "kubernetes_secret" "flux_git_deploy" {
  metadata {
    name      = local.git_secret_name
    namespace = kubernetes_namespace.flux_system.metadata[0].name
    labels = {
      shared-by-flux = true
    }
  }
  data = {
    identity = tls_private_key.flux_ssh_key.private_key_pem
    public   = tls_private_key.flux_ssh_key.public_key_openssh
  }
  depends_on = [
    kubernetes_namespace.flux_system,
    tls_private_key.flux_ssh_key
  ]

  # The resource tls_private_key.flux_ssh_key will create a new
  # key, every time terraform apply is ran. 
  # We need to avoid changing the contents of the secret.
  lifecycle {
    ignore_changes = [
      data,
    ]
  }
}

# ---------------------------------------------------------------------
# Configmap with private key for flux connection to Gitlab Repositories
# ---------------------------------------------------------------------
resource "kubernetes_config_map" "flux_git_deploy" {
  metadata {
    name      = "flux-git-deploy-configmap"
    namespace = kubernetes_namespace.flux_system.metadata[0].name
  }
  data = {
    "flux-git-deploy-values.yaml" = "${templatefile("${path.module}/tf-templates/flux-git-deploy.tmpl", {
      identity = tls_private_key.flux_ssh_key.private_key_pem
      public   = tls_private_key.flux_ssh_key.public_key_openssh
    })}"
  }
  depends_on = [
    kubernetes_namespace.flux_system,
    tls_private_key.flux_ssh_key
  ]
  lifecycle {
    ignore_changes = [data]
  }
}

# ---------------------------------------------
# Secret with Gitlab Image Registry credentials
# ---------------------------------------------
resource "kubernetes_secret" "gitlab_registry" {
  type = "kubernetes.io/dockerconfigjson"
  metadata {
    name      = "gitlab-registry"
    namespace = kubernetes_namespace.flux_system.metadata[0].name
    labels = {
      shared-by-flux = true
    }
  }
  data = {
    ".dockerconfigjson" = "${templatefile("${path.module}/tf-templates/gitlab-registry-dockerconfig.tmpl",
      {
        gitlab_registry_username = var.gitlab_registry_username,
        gitlab_registry_password = var.gitlab_registry_password
      }
    )}"
  }
}

# -----------------------------
# Values for flux in namespaces
# -----------------------------
resource "kubernetes_config_map" "flux_microservice_releases_repo" {
  metadata {
    name      = "flux-microservice-releases-repo"
    namespace = kubernetes_namespace.flux_system.metadata[0].name
  }
  data = {
    "flux-configmap" = "${templatefile("${path.module}/tf-templates/flux-configmap.tmpl", {
      flux_microservice_releases_git_branch = var.flux_microservice_releases_git_branch,
      terraform_workspace                   = terraform.workspace
    })}"
  }
  depends_on = [
    kubernetes_namespace.flux_system
  ]
}

# ------------------------------------------------------
# Kubernetes policy for allowing egress traffic for flux
# ------------------------------------------------------
resource "kubernetes_network_policy" "allow_flux_egress" {
  metadata {
    name      = "allow-flux-egress"
    namespace = kubernetes_namespace.flux_system.metadata[0].name
  }
  spec {
    pod_selector {
      match_expressions {
        key      = "name"
        operator = "In"
        values   = ["flux"]
      }
    }
    egress {} # single empty rule to allow all egress traffic
    policy_types = ["Egress"]
  }
}

# --------------------------------------
# Install flux in flux-system namespace
# --------------------------------------
resource "helm_release" "flux_system" {
  name          = "flux"
  repository    = "https://charts.fluxcd.io"
  chart         = "flux"
  namespace     = kubernetes_namespace.flux_system.metadata[0].name
  atomic        = false
  version       = "1.4.1"
  reuse_values  = false
  recreate_pods = true
  wait          = false
  values = [
    "${file("helm-values/flux.yaml")}"
  ]
  set {
    type  = "string"
    name  = "git.secretName"
    value = local.git_secret_name
  }
  set {
    type  = "string"
    name  = "git.branch"
    value = var.flux_cluster_git_branch
  }
  set {
    type  = "string"
    name  = "git.path"
    value = var.flux_cluster_git_path
  }
  set {
    type  = "string"
    name  = "git.label"
    value = "${terraform.workspace}-${var.flux_cluster_git_branch}-flux-sync"
  }
  depends_on = [
    kubernetes_secret.flux_git_deploy
  ]
}

# ----------------------------------------------
# Install helm operator in flux-system namespace
# ----------------------------------------------
resource "helm_release" "helm_operator" {
  name          = "helm-operator"
  repository    = "https://charts.fluxcd.io"
  chart         = "helm-operator"
  namespace     = kubernetes_namespace.flux_system.metadata[0].name
  atomic        = false
  version       = "1.1.0"
  reuse_values  = false
  recreate_pods = true
  wait          = false
  values = [
    "${file("helm-values/helm-operator.yaml")}"
  ]
  set {
    type  = "string"
    name  = "git.ssh.secretName"
    value = local.git_secret_name
  }
  depends_on = [
    kubernetes_secret.flux_git_deploy
  ]
}