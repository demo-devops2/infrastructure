# -----------
# GKE cluster
# -----------
module "demo_devops_cluster" {
  source                            = "github.com/terraform-google-modules/terraform-google-kubernetes-engine//modules/beta-private-cluster"
  project_id                        = var.gcp_project_id
  name                              = join("-", ["demo-devops-cluster", terraform.workspace])
  regional                          = true
  region                            = var.gcp_region
  network                           = module.gke_cluster_network_config.network_name
  subnetwork                        = module.gke_cluster_network_config.subnets_names[0]
  ip_range_pods                     = var.gcp_vpc_ip_range_pods_name
  ip_range_services                 = var.gcp_vpc_ip_range_services_name
  network_policy                    = true
  horizontal_pod_autoscaling        = true
  service_account                   = "create"
  enable_private_endpoint           = false
  enable_private_nodes              = true
  maintenance_start_time            = "2020-03-16T03:00:00Z"
  maintenance_end_time              = "2020-03-16T10:00:00Z"
  maintenance_recurrence            = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU"
  release_channel                   = "REGULAR"
  disable_legacy_metadata_endpoints = true
  remove_default_node_pool          = true
  master_ipv4_cidr_block            = "172.16.0.0/28"

  master_authorized_networks = [
    {
      cidr_block   = "0.0.0.0/0"
      display_name = "World"
    }
  ]

  istio              = var.gke_istio
  istio_auth         = var.gke_istio_strict_mtls ? "AUTH_MUTUAL_TLS" : "AUTH_NONE"
  dns_cache          = false
  logging_service    = var.gke_logging_and_monitoring ? "logging.googleapis.com/kubernetes" : "none"
  monitoring_service = var.gke_logging_and_monitoring ? "monitoring.googleapis.com/kubernetes" : "none"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-2"
      min_count          = 1
      max_count          = 3
      disk_size_gb       = 10
      disk_type          = "pd-ssd"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = var.gke_preemtible
      initial_node_count = 1
    }
  ]
}

# -------------------------
# GKE cluster client config
# -------------------------
data "google_client_config" "default" {
}

# ----------------
# GKE cluster data
# ----------------
data "google_container_cluster" "demo_devops_cluster" {
  name     = join("-", [var.gke_cluster_name, terraform.workspace])
  project  = var.gcp_project_id
  location = var.gcp_region
  depends_on = [
    module.demo_devops_cluster
  ]
}
