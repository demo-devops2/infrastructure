# ---------------------------------
# GKE cluster network configuration
# ---------------------------------
module "gke_cluster_network_config" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 2.5"
  project_id   = var.gcp_project_id
  network_name = join("-", [var.gcp_vpc_network, terraform.workspace])

  subnets = [
    {
      subnet_name           = join("-", [var.gcp_vpc_subnetwork, terraform.workspace])
      subnet_ip             = "10.0.0.0/16"
      subnet_region         = var.gcp_region
      subnet_private_access = true
    },
  ]

  secondary_ranges = {
    "${join("-", [var.gcp_vpc_subnetwork, terraform.workspace])}" = [
      {
        range_name    = var.gcp_vpc_ip_range_pods_name
        ip_cidr_range = "10.16.0.0/20"
      },
      {
        range_name    = var.gcp_vpc_ip_range_services_name
        ip_cidr_range = "10.1.0.0/17"
      },
    ]
  }
}

# The cluster is private, so we need to create a CloudNAT to allow the cluster
# reach out internet.

resource "google_compute_address" "address" {
  count   = 1
  name    = join("-", ["nat-manual-ip-${count.index}", terraform.workspace])
  project = var.gcp_project_id
  region  = var.gcp_region
}

resource "google_compute_router" "router" {
  name    = join("-", ["router", terraform.workspace])
  project = var.gcp_project_id
  region  = var.gcp_region
  network = module.gke_cluster_network_config.network_self_link
}

resource "google_compute_router_nat" "nat_manual" {
  name    = join("-", ["router-nat", terraform.workspace])
  router  = google_compute_router.router.name
  project = var.gcp_project_id
  region  = google_compute_router.router.region

  nat_ip_allocate_option = "MANUAL_ONLY"
  nat_ips                = google_compute_address.address.*.self_link

  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = module.gke_cluster_network_config.subnets_self_links[0]
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}
