terraform {
  # Use a GCS Bucket as a backend
  backend "gcs" {}
}

provider "google-beta" {
  project = var.gcp_project_id
  region  = var.gcp_region
  version = ">= 3.19.0"
}

provider "kubernetes" {
  load_config_file = false
  host             = "https://${module.demo_devops_cluster.endpoint}"
  cluster_ca_certificate = trimspace(
    base64decode(
      module.demo_devops_cluster.ca_certificate,
    )
  )
  token = data.google_client_config.default.access_token
}

provider "helm" {
  version = ">= 1.2.1"
  kubernetes {
    load_config_file = false
    host             = "https://${module.demo_devops_cluster.endpoint}"
    cluster_ca_certificate = trimspace(
      base64decode(
        module.demo_devops_cluster.ca_certificate,
      )
    )
    token = data.google_client_config.default.access_token
  }
}

provider "gitlab" {
}